package com.luv2code.springdemo;

public class TaekwondoCoach implements Coach {

	//	Define a private field for the dependency
	private FortuneService fortuneService;
	
	//	Define a constructor for the dependency injection
	public TaekwondoCoach(FortuneService theFortuneService) {
		fortuneService = theFortuneService;
	}
	
	@Override
	public String getDailyWorkout() {
		return "Practice Kicks for half an hour!";
	}

	@Override
	public String getDailyFortune() {
		// use my fortuneService to get a fortune
		return fortuneService.getFortune();
	}

}
