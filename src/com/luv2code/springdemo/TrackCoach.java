package com.luv2code.springdemo;

public class TrackCoach implements Coach {

//	Define a private field for the dependency
	private FortuneService fortuneService;
	
	public TrackCoach() {
		
	}
	
	public TrackCoach(FortuneService fortuneService) {
		this.fortuneService = fortuneService;
	}

	@Override
	public String getDailyWorkout() {
		
		return "Run a hard 5k";
	}

	@Override
	public String getDailyFortune() {
		return "Just DEW IT! "+fortuneService.getFortune();
	}

	// add init method
	public void doMyStartupStuff(){
		System.out.println("TrackCoach: inside method doMyStartupStuff");
	}
	// add destroy method
	public void doMyCleanupStuffYoYo(){
		System.out.println("TrackCoach: inside method doMyCleanupStuffYoYo");
	}


}
